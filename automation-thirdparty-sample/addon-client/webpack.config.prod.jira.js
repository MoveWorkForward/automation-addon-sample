const assign = require('object-assign');
const config = require('./webpack.config.dev.jira');

module.exports = assign({}, config, {
  mode: "production",
});
