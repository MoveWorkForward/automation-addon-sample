# Automation for Jira extension client developer guide

This guide explains how to provide the front-end for a third party pluggable trigger, condition or action in Automation
for Jira.

To provide a richer and easier to use experience for our users, we render the configuration UI entirely on the client-side using React.  Automation for Jira provides a public client-side API 
that third party vendors can use to register their config UI components with Automation for Jira.

**Note:** As of version 6.0.0 of Automation for Jira, third party extensions no longer have to use React when implementing the front-end. You can simply render your UI into the DOM elements provided via the public API.  This example is still based on React however since Atlaskit also uses React, so it's the logical choice. 

We build these components using the  latest tools of the trade:

* Yarn for JS package management
* [Atlaskit](https://atlaskit.atlassian.com/) UI components provided by Atlassian
* React
* Webpack for packaging everything up

The fully package JS files are then included in the atlassian plugin JAR using a standard web-resource
module descriptor in the `atlassian-plugin.xml`.  This has to be added to the `<context>automation-addons-context</context>`
for inclusion in the Automation for Jira pages.

## Anatomy of the client-side UI of an extension

There's a number of components you need to consider when implementing an Automation for Jira UI:

* The main component definition (e.g. `project-created-trigger.js`): This defines the type of rule component, initial parameter values, its renderer and parameter validation
* The config component (e.g. `project-created-config-form.js`): This renders the configuration UI on the right hand side

Depending on how complex your UI is, the main component definition could be split into further components.  For example 
you might want to introduce another component to render the 'summary' view.

Configuration can be any JSON object of your choosing.  For example, you might store config like this for your component:

```json
{
  "projectDescription": "Hello world: {{createdProject.key}}",
  "meta": {
     "extra": "stuff"
  }
}
```

This is exactly how this will be provided to your component on the server side via the `ruleContext.getComponentConfigBean().getValue()` accessor.  You'll have to then deal with deserializing this JSON string
into a Java class yourself (though we'd recommend to use Jackson).

Please also note that the client-side UI renders inside an iframe (both in Server and Cloud). This means when you include links you need to add these 
link attributes so they open in the parent window: `target="_blank" rel="noopener noreferrer"`


## Public client-side API provided by Automation for Jira 

We provide the following public API in the `CodeBarrel` global namespace:

```javascript
CodeBarrel.Automation = {
  /**
   * Registers the provided component for rendering by Automation for Jira.  The provided component's type attribute
   * needs to match the <automation-rule-component/> complete module key from the server.
   *
   * @param componentDefinition the full component details to register. See below for full details 
   */
  registerComponent: (componentDefinition) => {},
  
  /**
   * Updates the provided's component with new configuration parameters.  Note that this does not persist to the server
   * but instead will store this new value simply client side.  Only publishing the Automation rule persists the state to
   * the server.
   * 
   * @param config the existing config for a component
   * @param newValue the new configuration value to store
   */
  updateComponentValue: (config, newValue) => { },
  
  /**
   * Returns all validation errors that are relevant to the current component.
   *
   * @param component the current component
   */
  componentErrors: (config) => { },
  
  /**
   * Returns the current context.  This object contains baseUrl (for REST calls), user permission details, current project details and
   * if we are rendering the project or global admin UI. It also contains all configured fields in Jira.
   */
  getContext: () => { },
  
  /**
   * Returns if we are currently in a read only state (e.g. if the rule is in progress of being published to the server)
   */
  isReadOnly: () => { },
};
```

### Component Definition to pass to `registerComponent(componentDefinition)`

Components are defined as follows:
```javascript
const myComponent = {
  /** 
  * This has to match the server side 'automation-action' complete module key: <pluginkey>:<automation-action-modulekey> 
  */
  type: '<pluginkey>:<automation-action-modulekey>',

  /** Defines  what type of component this is */
  componentType: 'TRIGGER|CONDITION|ACTION',

  /** this renders the name in the header of the config form and when selecting a new action */
  name: () => "Return an i18nized name here",

  /**
   * Use SVG icons only please.  Transparent background 36x36 pixels with white line color only.  Your icon should *not* look out of place
   * or stand out in any special way compare to other icons used in Automation for Jira!
   */
  icon: () => '../<pluginkey>:<module>key>/path/to/your/icon.svg',

  /** Defines the JSON initial config for this component. Can just be null if there is none. */
  initialValue: () => ({}),

  getRenderer: () => ({
    /** this renders the header in the sidebar.  Then: xxxx */
    summaryLabel: (config, context, containerEl) => { /* render i18nized summaryLabel into containerEl */},

    /** This renders the contents of the panel in the sidebar. Should show short summary of configured options */
    renderSummary: (config, context, containerEl) => { /* render i18nized summary into containerEl */ },

    /** This renders the detailed config form on the right hand side in the rule builder */
    renderDetailed: (config, context, containerEl) => { /* render config form into containerEl */ },

    /** this renders the description when selecting a new action. Should be short description. */
    renderCard: (containerEl) => { /* render i18nized card into containerEl */},
  }),

  /**
   * Us this to carry out client side validation. You must also validate on the server, but this
   * is useful to give the user immediate feedback when they click 'Save' client-side before publishing
   */
  validate: (rule, config) => { 
    // Return JSON object with field validation errors here
    return { fieldKey: "Eror message"}
  },

  /**
   * Set this to false, if this component doesn't require a config UI.
   */
  isEditable: false,

  /**
   * Valid categories are: 'ISSUE_ACTIONS','SERVICE_DESK_ACTION','ISSUE_TRIGGERS','NOTIFICATIONS','SCHEDULED','INTEGRATIONS'
   * This is used for the search dropdown in the select action ui.
   */
  categories: [],

  /**
   * Add additional labels to aid searching.  These wont appear in the UI, but will match text search when selecting an action.
   * Only use labels that aren't included already in your name or card.
   */
  labels: [],
};
```

## Lifecycle

As soon as the component is defined, it needs to register itself using the `window.CodeBarrel.Automation.registerComponent(ProjectCreatedTriggerComponent);`  
public API (see `index.js`).  This lets Automation for Jira know that your providing this pluggable UI linked to the `type` attribute
you specified in your component (which links to the server side `<automation-rule-component/>`).

As soon as users update UI fields (e.g. type a character in an input field), you should trigger `updateComponentValue()`
with the new config values.  This will cause a re-render of the React component so you can provide live previews in the 
summary view on the left hand side in the rule sidebar.

When users hit 'Save' a call to your `validate()` (see `project-condition.js`) method will be made giving you a chance to perform client-side validation.

To access errors for your component you can use `componentErrors()`.  

When users hit 'Publish rule' we take care of saving the latest state (last update provided by `updateComponentValue()`) to the server.
If there were validation errors on the server, then the config UI will be asked to re-render.  Server side errors for your
component are also accessible via `componentErrors()`.

