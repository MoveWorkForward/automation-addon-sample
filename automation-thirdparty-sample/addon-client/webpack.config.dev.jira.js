const path = require('path');

module.exports = {
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
          },
        },
      },
    ],
  },
  entry: {
    thirdpartysample: './src/index.js'
  },
  output: {
    path: path.join(__dirname, '../src/main/resources/client'),
    filename: '[name].pack.js',
  },
  externals: {
    'styled-components': 'window.CodeBarrel.StyledComponents',
  },
  devtool: 'cheap-source-map',
  mode: 'development',
};