import React from 'react';
import ReactDOM from 'react-dom';
import ProjectConditionConfigForm from './project-condition-config-form';

const ProjectCategoryConditionComponent = {
  /** This has to match the server side 'automation-action' complete module key: <pluginkey>:<automation-action-modulekey> */
  type: 'com.codebarrel.automation.addon.automation-thirdparty-sample:project-condition',

  /** Can be one of TRIGGER|CONDITION|ACTION */
  componentType: 'CONDITION',

  /** this renders the name in the header of the config form and when selecting a new action */
  name: () => AJS.I18n.getText('com.codebarrel.thirdparty.addon.project.condition.name'),

  /**
   * Use SVG icons only please.  Transparent background 36x36 pixels with white line color only.  Your icon should *not* look out of place
   * or stand out in any special way compare to other icons used in Automation for Jira!
   */
  icon: () => '../com.codebarrel.automation.addon.automation-thirdparty-sample:sample-component-resources/images/sample-icons/comment.svg',

  /** Components only support values of the form { parameters: { yourKey: [yourValue(s)] } }. */
  initialValue: () => ({ nameRegex: '' }),

  getRenderer: () => ({
    /** this renders the header in the sidebar.  Then: xxxx */
    summaryLabel: (config, context, containerEl) => AJS.I18n.getText('com.codebarrel.thirdparty.addon.project.condition.summary'),

    /** This renders the contents of the panel in the sidebar. Should show short summary of configured options */
    renderSummary: (config, context, containerEl) => {
      ReactDOM.render(<span>{config.value.nameRegex ? config.value.nameRegex : ''}</span>, containerEl);
    },

    /** This renders the detailed config form on the right hand side in the rule builder */
    renderDetailed: (config, context, containerEl) => {
      ReactDOM.render(<ProjectConditionConfigForm config={config} />, containerEl);
    },

    /** this renders the description when selecting a new action. Should be short description. */
    renderCard: (containerEl) => {
      ReactDOM.render(
        <span>{AJS.I18n.getText('com.codebarrel.thirdparty.addon.project.condition.card')}</span>, containerEl);
    },
  }),

  /**
   * Us this to carry out client side validation. You must also validate on the server, but this
   * is useful to give the user immediate feedback when they click 'Save' client-side before publishing
   */
  validate: (rule, config) => {
    if (!config.value.nameRegex) {
      return { nameRegex: "Please enter a regex" };
    }
    return {}
  },

  /**
   * Set this to false, if this component doesn't require a config UI.
   */
  isEditable: false,

  /**
   * Valid categories are: 'ISSUE_ACTIONS','SERVICE_DESK_ACTION','ISSUE_TRIGGERS','NOTIFICATIONS','SCHEDULED','INTEGRATIONS'
   * This is used for the search dropdown in the select action ui.
   */
  categories: [],

  /**
   * Add additional labels to aid searching.  These wont appear in the UI, but will match text search when selecting an action.
   * Only use labels that aren't included already in your name or card.
   */
  labels: [],
};

export default ProjectCategoryConditionComponent;