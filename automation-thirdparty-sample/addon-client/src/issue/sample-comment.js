import React from 'react';
import ReactDOM from 'react-dom';
import SampleCommentConfigForm from './sample-comment-config';

const SampleCommentComponent = {
  type: 'com.codebarrel.automation.addon.automation-thirdparty-sample:new-sample-comment-issue-action',
  componentType: 'ACTION',
  name: () => AJS.I18n.getText('com.codebarrel.thirdparty.sample.comment.name'),
  icon: () => '../com.codebarrel.automation.addon.automation-thirdparty-sample:sample-component-resources/images/sample-icons/comment.svg',
  initialValue: () => ({ body: '', sendNotification: false }),
  getRenderer: () => ({
    summaryLabel: () => AJS.I18n.getText('com.codebarrel.thirdparty.sample.comment.summary'),
    renderSummary: (config, context, containerEl) => {
      let value = '';
      if (config.value && config.value.body) {
        value = config.value.body;
      }
      ReactDOM.render(<div title={value}>{value}</div>, containerEl);
    },
    renderDetailed: (config, context, containerEl) => {
      ReactDOM.render(<SampleCommentConfigForm config={config} />, containerEl);
    },
    renderCard: (containerEl) => {
      ReactDOM.render(<span>{AJS.I18n.getText('com.codebarrel.thirdparty.sample.comment.card')}</span>, containerEl);
    },
  }),
  validate: (rule, config) => {
    const commentBody = config.value.body;
    //this is a contrived example!
    if (commentBody.indexOf('Hello') === -1) {
      return { body: AJS.I18n.getText('com.codebarrel.thirdparty.sample.comment.client.error') }
    }
    return { body: null };
  },
  isEditable: true,
  categories: [],
  labels: [],
};

export default SampleCommentComponent;
