import React from 'react';

/**
 * Example of a minimal trigger UI implementation.  Should be i18nized but that's left as an exercise to the reader.
 */
const IssueCreatedTriggerComponent = {
  type: 'com.codebarrel.automation.addon.automation-thirdparty-sample:issue-created-trigger',
  componentType: 'TRIGGER',
  name: () => "Custom issue created trigger",
  icon: () => '../com.codebarrel.automation.addon.automation-thirdparty-sample:sample-component-resources/images/sample-icons/comment.svg',
  initialValue: () => null,
  getRenderer: () => ({
    summaryLabel: (config, context, containerEl) => "Issue created (custom)",
    renderSummary: (config, context, containerEl) => null,
    renderDetailed: (config, context, containerEl) => null,
    renderCard: (containerEl) => "Example of thirdparty issue created trigger",
  }),
  validate: (rule, config) => ({}),
  categories: [],
  labels: [],
};

export default IssueCreatedTriggerComponent;