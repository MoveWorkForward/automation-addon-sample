import React from 'react';

const IssueKeyEvenConditionComponent = {
  type: 'com.codebarrel.automation.addon.automation-thirdparty-sample:issue-key-even-condition',
  componentType: 'CONDITION',
  name: () => "Issue key even condition",
  icon: () => '../com.codebarrel.automation.addon.automation-thirdparty-sample:sample-component-resources/images/sample-icons/comment.svg',
  initialValue: () => null,
  getRenderer: () => ({
    summaryLabel: (config, context, containerEl) => "Issue key even condition",
    renderSummary: (config, context, containerEl) => null,
    renderDetailed: (config, context, containerEl) => null,
    renderCard: (containerEl) => "Checks if the key of an issue is even",
  }),
  validate: (rule, config) => ({}),
  categories: [],
  labels: [],
};

export default IssueKeyEvenConditionComponent;