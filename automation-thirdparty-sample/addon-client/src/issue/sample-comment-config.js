import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { ErrorMessage, Field } from '@atlaskit/form';
import Textarea from '@atlaskit/textarea';
import Toggle from '@atlaskit/toggle';
import styled from 'styled-components';

const FormContainer = styled.div`
  padding-bottom:5px;
`;

export const SampleCommentConfigForm = (props) => {
  const config = props.config;
  const componentErrors = window.CodeBarrel.Automation.componentErrors(config);
  const disabled = window.CodeBarrel.Automation.isReadOnly();

  const sendNotification = !!config.value.sendNotification;

  return (
    <FormContainer>
      <p>{AJS.I18n.getText('com.codebarrel.thirdparty.sample.comment.config.desc')}</p>
      <Field
        name="body"
        label={AJS.I18n.getText('com.codebarrel.thirdparty.sample.comment.config.label')}
        isDisabled={disabled}
        isInvalid={!!componentErrors.body}
      >
        {({ fieldProps }) => (
          <Fragment>
            <Textarea
              {...fieldProps}
              value={config.value.body}
              onChange={(e) => {
                window.CodeBarrel.Automation.updateComponentValue(config, { ...config.value, body: e.target.value });
              }}
              placeholder={AJS.I18n.getText('com.codebarrel.thirdparty.sample.comment.config.placeholder')}
              minimumRows={5}
            />
            {componentErrors.body ?
              <ErrorMessage>{componentErrors.body}</ErrorMessage> : null}
          </Fragment>)
        }
      </Field>
      <Field
        name="sendNotification"
        label={AJS.I18n.getText('com.codebarrel.thirdparty.sample.notification.label')}
        isDisabled={disabled}
        isInvalid={!!componentErrors.sendNotification}
      >
        {({ fieldProps }) => (
          <Toggle
            {...fieldProps}
            isDefaultChecked={sendNotification}
            onChange={() => window.CodeBarrel.Automation.updateComponentValue(config, {
              ...config.value,
              sendNotification: !sendNotification
            })}
          />)
        }
      </Field>
    </FormContainer>
  );
};

SampleCommentConfigForm.propTypes = {
  config: PropTypes.object.isRequired,
};

export default SampleCommentConfigForm;
