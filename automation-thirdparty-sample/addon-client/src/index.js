import ProjectCreatedTriggerComponent from './project/project-created-trigger';
import ProjectConditionComponent from './project/project-condition';
import ProjectEditActionComponent from './project/project-edit-action';
import IssueCreatedTriggerComponent from './issue/issue-created-trigger'
import IssueKeyEvenConditionComponent from './issue/issue-key-even-condition';
import SampleCommentComponent from './issue/sample-comment';

window.CodeBarrel.Automation.registerComponent(ProjectCreatedTriggerComponent);
window.CodeBarrel.Automation.registerComponent(ProjectConditionComponent);
window.CodeBarrel.Automation.registerComponent(ProjectEditActionComponent);

window.CodeBarrel.Automation.registerComponent(IssueCreatedTriggerComponent);
window.CodeBarrel.Automation.registerComponent(IssueKeyEvenConditionComponent);

window.CodeBarrel.Automation.registerComponent(SampleCommentComponent);