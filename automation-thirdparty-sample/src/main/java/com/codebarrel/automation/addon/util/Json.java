package com.codebarrel.automation.addon.util;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

public class Json {
    private static final Logger LOG = Logger.getLogger(Json.class);
    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static String toJson(Object o) {
        try {
            return objectMapper.writeValueAsString(o);
        } catch (IOException e) {
            LOG.error("Error serializing object '" + o + "' to JSON.", e);
            throw new RuntimeException(e);
        }
    }

    public static <T> T toObject(String input, Class<T> type) {
        try {
            return objectMapper.readValue(input, type);
        } catch (IOException e) {
            LOG.error("Error deserializing input '" + input + "' to object '" + type + "'.", e);
            throw new RuntimeException(e);
        }
    }

    public static <T> T convertObject(Object input, Class<T> type) {
        return objectMapper.convertValue(input, type);
    }
}
