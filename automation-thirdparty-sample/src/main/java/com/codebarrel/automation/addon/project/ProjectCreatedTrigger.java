package com.codebarrel.automation.addon.project;

import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.event.ProjectCreatedEvent;
import com.atlassian.jira.project.Project;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.codebarrel.automation.addon.model.ProjectCreatedConfig;
import com.codebarrel.automation.addon.model.ProjectIdentity;
import com.codebarrel.automation.addon.util.Json;
import com.codebarrel.automation.api.config.ComponentConfigBean;
import com.codebarrel.automation.api.thirdparty.EventTriggerRuleComponent;
import com.codebarrel.automation.api.thirdparty.audit.AuditItemAssociatedType;
import com.codebarrel.automation.api.thirdparty.context.ComponentInputs;
import com.codebarrel.automation.api.thirdparty.context.ErrorCollection;
import com.codebarrel.automation.api.thirdparty.context.ErrorCollectionImpl;
import com.codebarrel.automation.api.thirdparty.context.RuleContext;
import com.codebarrel.automation.api.thirdparty.context.result.ExecutionResult;
import com.codebarrel.automation.api.thirdparty.context.result.ExecutionResultBuilder;
import com.codebarrel.automation.api.thirdparty.smartvalues.SmartValueContextProvider;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Scanned
public class ProjectCreatedTrigger implements EventTriggerRuleComponent {
    private static final Set<String> HANDLED_EVENTS = Sets.newHashSet(ProjectCreatedEvent.class.getName());
    private static final Set<String> VALID_PROJECT_TYPES = Sets.newHashSet("business", "software", "servicedesk");

    public static final String PROJECT_TYPE = "projectType";
    public static final String PROJECT_NAME = "projectName";

    private final ProjectService projectService;

    @Inject
    public ProjectCreatedTrigger(@ComponentImport ProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public Set<String> getHandledEventTypes() {
        return HANDLED_EVENTS;
    }

    /**
     * Not every trigger needs to implement this. If your events are global and don't apply to a specific project, then
     * this method doesn't need to be implemented.
     * <p>
     * It does mean however that your rules will have be defined as global rules (see the 'Restrict to projects' setting in 'Rule details' in the UI).
     */
    @Override
    public Set<String> getProjectContext(Object event) {
        // it's possible for a trigger to listen to multiple event types so this instanceof check is necessary.
        if (event instanceof ProjectCreatedEvent) {
            return Sets.newHashSet(Long.toString(((ProjectCreatedEvent) event).getId()));
        }
        return Collections.emptySet();
    }

    /**
     * This shows how you could serialize only the information you care about for this trigger to JSON.
     * <p>
     * You could also just return the project id here and fetch full project details later
     * in the {@link #execute(RuleContext, ComponentInputs)} method.  This would add more load on Jira though since this
     * rule would be fetching information that's already been provided in the event again later.
     * <p>
     * Automation rules may run *a lot* so always consider the performance implications in your rule components!
     */
    @Override
    public String serializeEvent(Object event) {
        if (event instanceof ProjectCreatedEvent) {
            final ProjectCreatedEvent projectEvent = (ProjectCreatedEvent) event;
            final ProjectIdentity projectIdentity = new ProjectIdentity(projectEvent.getId(), projectEvent.getProject().getKey());
            return Json.toJson(projectIdentity);
        }
        return null;
    }

    /**
     * By default *not everything* is available to smart-values ({{blah}}). To make additional items available for
     * smart-value rendering you can implement a contextProvider like this, that pulls stuff out of your inputs and
     * puts them into context for smart-value rendering.
     * <p>
     * For example the below makes {{createdProject}} available in smart-values (and all fields marked with
     * {@link com.codebarrel.automation.api.thirdparty.smartvalues.SmartValueSafe} in {@link ProjectIdentity}, e.g. {{createdProject.id}})
     */
    @Override
    public Optional<SmartValueContextProvider> getSmartValueContextProvider() {
        return Optional.of((existingContext, componentInputs) -> {
            try {
                final Map<String, Object> ret = new HashMap<>();
                final ProjectIdentity projectIdentity = Json.toObject(componentInputs.getSerializedEvent(), ProjectIdentity.class);
                ret.put("createdProject", projectIdentity);
                ret.put("createdProjectType", componentInputs.getInputs().get(PROJECT_TYPE));
                return ret;
            } catch (RuntimeException e) {
                // might be inputs provided by another trigger so this serialization might fail.
                // there's probably smarter ways you could do this (checking for specific key in inputs etc)

                return Collections.emptyMap();
            }
        });
    }

    @Override
    public ErrorCollection validateConfig(RuleContext context, ComponentConfigBean config) {
        final ErrorCollection errors = new ErrorCollectionImpl();
        final ProjectCreatedConfig triggerConfig = Json.convertObject(config.getValue(), ProjectCreatedConfig.class);
        if (StringUtils.isNotBlank(triggerConfig.getProjectType())) {
            // Note: Ideally you should probably retrieve the available project types from Jira via API calls, but that's
            //       left as an exercise for the reader ;).
            if (!VALID_PROJECT_TYPES.contains(triggerConfig.getProjectType())) {
                errors.addError(PROJECT_TYPE, context.getI18n().getText("com.codebarrel.thirdparty.addon.project.created.project.type.error"));
            }
        }
        return errors;
    }

    /**
     * This is really the guts of a third party component.  In the case of a trigger you can lookup related items and
     * populate the inputs for other components in the rule chain.
     * <p>
     * Conditions can stop execution.
     * <p>
     * Actions can perform changes (based on the config etc).
     */
    @Override
    public ExecutionResult execute(RuleContext ruleContext, ComponentInputs componentInputs) {
        final ProjectIdentity projectIdentity = Json.toObject(componentInputs.getSerializedEvent(), ProjectIdentity.class);

        // this is a bit contrived - could have also just stored more info into the ProjectIdentity class earlier in serializeEvent(event), but
        // just showing how you could look up additional stuff from services in Jira here.
        final ProjectService.GetProjectResult result = projectService.getProjectById(projectIdentity.getId());
        if (result.isValid()) {
            final Project project = result.get();
            final ProjectCreatedConfig triggerConfig = Json.convertObject(ruleContext.getComponentConfigBean().getValue(), ProjectCreatedConfig.class);
            if (StringUtils.isNotBlank(triggerConfig.getProjectType()) && !project.getProjectTypeKey().getKey().equals(triggerConfig.getProjectType())) {
                ruleContext.getAuditLog().addMessage("com.codebarrel.thirdparty.addon.project.created.type.mismatch");
                return ExecutionResultBuilder.stopRule();
            }

            ruleContext.getAuditLog().addMessage("com.codebarrel.thirdparty.addon.project.created.audit.new", String.valueOf(projectIdentity.getKey()));
            ruleContext.getAuditLog().addAssociatedItem(AuditItemAssociatedType.PROJECT, String.valueOf(projectIdentity.getId()), projectIdentity.getKey());

            final Map<String, Serializable> additionalInputs = new HashMap<>();
            // NOTE: Type information of the value you store here may be lost. We serialize anything in the inputs so it can go
            // into our Database queue, so this may come back as just a Map<> in componentInputs in subsequent rule components
            additionalInputs.put(PROJECT_TYPE, project.getProjectTypeKey().getKey());
            additionalInputs.put(PROJECT_NAME, project.getName());
            return ExecutionResultBuilder.continueRuleWithAdditionalInputs(additionalInputs);
        }

        //should never happen
        ruleContext.getAuditLog().addError("com.codebarrel.thirdparty.addon.project.created.no.project", String.valueOf(projectIdentity.getKey()));
        return ExecutionResultBuilder.stopRule();
    }
}
