package com.codebarrel.automation.addon.model;

public class ProjectCreatedConfig {
    private String projectType;

    public ProjectCreatedConfig() {
    }

    public ProjectCreatedConfig(String projectType) {
        this.projectType = projectType;
    }

    public String getProjectType() {
        return projectType;
    }
}
