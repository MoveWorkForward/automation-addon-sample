package com.codebarrel.automation.addon.project;

import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.codebarrel.automation.addon.model.ProjectEditConfig;
import com.codebarrel.automation.addon.model.ProjectIdentity;
import com.codebarrel.automation.addon.util.Json;
import com.codebarrel.automation.api.thirdparty.AutomationRuleComponent;
import com.codebarrel.automation.api.thirdparty.context.ComponentInputs;
import com.codebarrel.automation.api.thirdparty.context.RuleContext;
import com.codebarrel.automation.api.thirdparty.context.result.ExecutionResult;
import com.codebarrel.automation.api.thirdparty.context.result.ExecutionResultBuilder;

import javax.inject.Inject;

import static com.codebarrel.automation.addon.project.ProjectCreatedTrigger.PROJECT_TYPE;

@Scanned
public class EditProjectAction implements AutomationRuleComponent {

    private final ProjectService projectService;
    private final JiraAuthenticationContext authenticationContext;

    @Inject
    public EditProjectAction(@ComponentImport ProjectService projectService,
                             @ComponentImport JiraAuthenticationContext authenticationContext) {
        this.projectService = projectService;
        this.authenticationContext = authenticationContext;
    }

    @Override
    public ExecutionResult execute(RuleContext ruleContext, ComponentInputs componentInputs) {
        if (!componentInputs.getInputs().containsKey(PROJECT_TYPE)) {
            ruleContext.getAuditLog().addMessage("com.codebarrel.thirdparty.addon.project.edit.no.project.in.context");
            return ExecutionResultBuilder.stopRule();
        }

        final ProjectIdentity projectIdentity = Json.toObject(componentInputs.getSerializedEvent(), ProjectIdentity.class);
        final ProjectService.GetProjectResult projectResult = projectService.getProjectById(projectIdentity.getId());
        if (projectResult.isValid()) {
            final ProjectEditConfig config = Json.convertObject(ruleContext.getComponentConfigBean().getValue(), ProjectEditConfig.class);
            final String renderedDescription = ruleContext.renderSmartValues(config.getProjectDescription());

            ProjectService.UpdateProjectRequest updateProjectRequest = new ProjectService.UpdateProjectRequest(projectResult.getProject());
            updateProjectRequest.description(renderedDescription);
            final ProjectService.UpdateProjectValidationResult updateResult = projectService.validateUpdateProject(authenticationContext.getLoggedInUser(), updateProjectRequest);
            if (updateResult.isValid()) {
                projectService.updateProject(updateResult);
                ruleContext.getAuditLog().addMessage("com.codebarrel.thirdparty.addon.project.edit.success");
                return ExecutionResultBuilder.continueRule();
            } else {
                ruleContext.getAuditLog().addError("com.codebarrel.thirdparty.addon.project.edit.error", updateResult.getErrorCollection().toString());
                return ExecutionResultBuilder.stopRule();
            }
        } else {
            ruleContext.getAuditLog().addError("com.codebarrel.thirdparty.addon.project.edit.no.project", String.valueOf(projectIdentity.getId()));
            return ExecutionResultBuilder.stopRule();
        }
    }
}
