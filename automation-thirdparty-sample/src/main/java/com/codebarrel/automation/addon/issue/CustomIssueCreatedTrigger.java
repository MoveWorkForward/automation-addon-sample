package com.codebarrel.automation.addon.issue;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.codebarrel.automation.addon.model.IssueEventInfo;
import com.codebarrel.automation.addon.util.Json;
import com.codebarrel.automation.api.thirdparty.EventTriggerRuleComponent;
import com.codebarrel.automation.api.thirdparty.context.ComponentInputs;
import com.codebarrel.automation.api.thirdparty.context.RuleContext;
import com.codebarrel.automation.api.thirdparty.context.result.ExecutionResult;
import com.codebarrel.automation.api.thirdparty.context.result.ExecutionResultBuilder;
import com.google.common.collect.Sets;

import javax.inject.Inject;
import java.util.Collections;
import java.util.Set;

@Scanned
public class CustomIssueCreatedTrigger implements EventTriggerRuleComponent {
    private static final Set<String> HANDLED_EVENTS = Sets.newHashSet(IssueEvent.class.getName());
    private static final int ISSUE_CREATED_TYPE_ID = 1;

    private final IssueService issueService;
    private final JiraAuthenticationContext authenticationContext;

    @Inject
    public CustomIssueCreatedTrigger(@ComponentImport final IssueService issueService,
                                     @ComponentImport final JiraAuthenticationContext authenticationContext) {
        this.issueService = issueService;
        this.authenticationContext = authenticationContext;
    }

    @Override
    public Set<String> getProjectContext(Object event) {
        if (event instanceof IssueEvent) {
            final Long projectId = ((IssueEvent) event).getProject().getId();
            return Sets.newHashSet(Long.toString(projectId));
        }
        return Collections.emptySet();
    }

    @Override
    public String serializeEvent(Object event) {
        if (event instanceof IssueEvent) {
            final IssueEvent issueEvent = (IssueEvent) event;
            final Long issueId = issueEvent.getIssue().getId();
            return Json.toJson(new IssueEventInfo(issueId, issueEvent.getEventTypeId()));
        }
        return null;
    }

    @Override
    public Set<String> getHandledEventTypes() {
        return HANDLED_EVENTS;
    }

    @Override
    public ExecutionResult execute(RuleContext ruleContext, ComponentInputs componentInputs) {
        final IssueEventInfo issueEventInfo = Json.toObject(componentInputs.getSerializedEvent(), IssueEventInfo.class);
        if (issueEventInfo.getEventTypeId() != ISSUE_CREATED_TYPE_ID) {
            return ExecutionResultBuilder.stopRule();
        }

        //NOTE: the authenticationContext.getLoggedInUser() here will be the actor user defined in 'Rule details'
        final IssueService.IssueResult issueResult = issueService.getIssue(authenticationContext.getLoggedInUser(), issueEventInfo.getIssueId());
        if (issueResult.isValid()) {
            // Adding the issue to the context.  Now all issue related actions in Automation for Jira (including built in ones will work too!)
            return ExecutionResultBuilder.withIssue(issueResult.getIssue()).build();
        }
        ruleContext.getAuditLog().addError("com.codebarrel.thirdparty.addon.issue.created.no.issue", String.valueOf(issueEventInfo.getIssueId()));
        return ExecutionResultBuilder.stopRule();
    }
}
