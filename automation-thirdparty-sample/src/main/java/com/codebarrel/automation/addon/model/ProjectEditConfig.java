package com.codebarrel.automation.addon.model;

public class ProjectEditConfig {
    private String projectDescription;

    public ProjectEditConfig() {
    }

    public ProjectEditConfig(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public String getProjectDescription() {
        return projectDescription;
    }
}
