package com.codebarrel.automation.addon.issue;

import com.atlassian.jira.issue.Issue;
import com.codebarrel.automation.api.thirdparty.AutomationRuleComponent;
import com.codebarrel.automation.api.thirdparty.context.ComponentInputs;
import com.codebarrel.automation.api.thirdparty.context.RuleContext;
import com.codebarrel.automation.api.thirdparty.context.result.ExecutionResult;
import com.codebarrel.automation.api.thirdparty.context.result.ExecutionResultBuilder;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;

import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * This condition demonstrates how a condition can filter issues from the inputs.
 * <p>
 * In server rules can run in 'bulk mode' if the checkbox is check on the trigger, where a list of issues will
 * be provided to subsequent components.  If a rule isn't running in bulk mode (the better default), then only a single issue
 * will be provided in the inputs (and the rule executes in parallel).
 */
public class IsKeyEvenIssueCondition implements AutomationRuleComponent {
    @Override
    public ExecutionResult execute(RuleContext ruleContext, ComponentInputs componentInputs) {
        final List<Issue> issues = componentInputs.getIssues();
        if (issues.isEmpty()) {
            ruleContext.getAuditLog().addError("com.codebarrel.thirdparty.addon.issue.condition.error");
            return ExecutionResultBuilder.stopRule();
        }

        final List<Issue> evenIssues = componentInputs.getIssues()
                .stream()
                .filter(issue -> issue.getNumber() % 2 == 0)
                .collect(toList());
        final List<Issue> issuesDidntMatch = Lists.newArrayList(componentInputs.getIssues());
        issuesDidntMatch.removeAll(evenIssues);
        if (!issuesDidntMatch.isEmpty()) {
            ruleContext.getAuditLog().addMessage("com.codebarrel.thirdparty.addon.issue.condition.nomatch", StringUtils.join(issuesDidntMatch, ","));
        }

        if (evenIssues.isEmpty()) {
            return ExecutionResultBuilder.stopRule();
        }
        // Note we'll pass along any matching issues here in 'bulk' since we already got them in this form.
        // Generally though other types of components (namely triggers) should only pass along single issues and call
        // queueExecution multiple times:
        //   return ExecutionResultBuilder
        //          .withIssues(issuesIntroducedByTrigger)
        //          .executeConcurrently();
        //
        // This will lead to parallel execution of subsequent components for those issus.
        // In some rare cases where you want to handle all issues in bulk you may need this though (e.g. actions
        // that send out bulk notifications)
        return ExecutionResultBuilder
                .withIssues(evenIssues)
                .executeBulk();
    }
}
