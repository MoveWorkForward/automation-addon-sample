package com.codebarrel.automation.addon.model;

public class IssueEventInfo {
    private long issueId;
    private long eventTypeId;

    public IssueEventInfo() {
    }

    public IssueEventInfo(long issueId, long eventTypeId) {
        this.issueId = issueId;
        this.eventTypeId = eventTypeId;
    }

    public long getIssueId() {
        return issueId;
    }

    public long getEventTypeId() {
        return eventTypeId;
    }
}
