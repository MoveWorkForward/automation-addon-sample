package com.codebarrel.automation.addon.issue;

import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.codebarrel.automation.addon.model.SampleCommentConfig;
import com.codebarrel.automation.addon.util.Json;
import com.codebarrel.automation.api.config.ComponentConfigBean;
import com.codebarrel.automation.api.thirdparty.AutomationRuleComponent;
import com.codebarrel.automation.api.thirdparty.context.ComponentInputs;
import com.codebarrel.automation.api.thirdparty.context.ErrorCollection;
import com.codebarrel.automation.api.thirdparty.context.ErrorCollectionImpl;
import com.codebarrel.automation.api.thirdparty.context.RuleContext;
import com.codebarrel.automation.api.thirdparty.context.result.ExecutionResult;
import com.codebarrel.automation.api.thirdparty.context.result.ExecutionResultBuilder;
import org.apache.commons.lang.StringUtils;

import javax.inject.Inject;

/**
 * This re-implementing the legacy SampleCommentIssueAction from automation-legacy-action-sample.
 * <p>
 * Just showing how the new API makes things cleaner!
 */
@Scanned
public class NewSampleCommentIssueAction implements AutomationRuleComponent {
    private static final String COMMENT_KEY = "body";

    private final JiraAuthenticationContext authenticationContext;
    private final CommentService commentService;

    @Inject
    public NewSampleCommentIssueAction(@ComponentImport JiraAuthenticationContext authenticationContext,
                                       @ComponentImport CommentService commentService) {
        this.authenticationContext = authenticationContext;
        this.commentService = commentService;
    }

    @Override
    public ErrorCollection validateConfig(RuleContext context, ComponentConfigBean config) {
        final SampleCommentConfig commentConfig = Json.convertObject(config.getValue(), SampleCommentConfig.class);

        final ErrorCollection errorCollection = new ErrorCollectionImpl();
        if (StringUtils.isBlank(commentConfig.getBody())) {
            errorCollection.addError(COMMENT_KEY, context.getI18n().getText("com.codebarrel.thirdparty.sample.comment.error.body"));
        }
        // contrived example.
        if (!StringUtils.contains(commentConfig.getBody(), "dear")) {
            errorCollection.addError(COMMENT_KEY, context.getI18n().getText("com.codebarrel.thirdparty.sample.comment.error.body.dear"));
        }
        return errorCollection;
    }

    @Override
    public ExecutionResult execute(RuleContext ruleContext, ComponentInputs componentInputs) {
        final SampleCommentConfig config = Json.convertObject(ruleContext.getComponentConfigBean().getValue(), SampleCommentConfig.class);

        final ApplicationUser user = authenticationContext.getLoggedInUser();
        for (Issue issue : componentInputs.getIssues()) {
            final CommentService.CommentParameters commentParameters = new CommentService.CommentParameters.CommentParametersBuilder()
                    .author(user)
                    .body(config.getBody())
                    .issue(issue)
                    .build();

            final CommentService.CommentCreateValidationResult validationResult = commentService.validateCommentCreate(user, commentParameters);
            if (validationResult.isValid()) {
                commentService.create(user, validationResult, config.isSendNotification());
                ruleContext.getAuditLog().addMessage("com.codebarrel.thirdparty.sample.comment.success", issue.getKey());
            } else {
                ruleContext.getAuditLog().addError("com.codebarrel.thirdparty.sample.comment.error", validationResult.getErrorCollection().toString());
                return ExecutionResultBuilder.stopRule();
            }
        }
        return ExecutionResultBuilder.continueRule();
    }
}
