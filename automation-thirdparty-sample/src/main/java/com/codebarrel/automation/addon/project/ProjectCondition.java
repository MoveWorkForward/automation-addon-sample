package com.codebarrel.automation.addon.project;

import com.codebarrel.automation.addon.model.ProjectConditionConfig;
import com.codebarrel.automation.addon.util.Json;
import com.codebarrel.automation.api.thirdparty.AutomationRuleComponent;
import com.codebarrel.automation.api.thirdparty.context.ComponentInputs;
import com.codebarrel.automation.api.thirdparty.context.RuleContext;
import com.codebarrel.automation.api.thirdparty.context.result.ExecutionResult;
import com.codebarrel.automation.api.thirdparty.context.result.ExecutionResultBuilder;
import org.apache.commons.lang.StringUtils;

import java.util.regex.Pattern;

import static com.codebarrel.automation.addon.project.ProjectCreatedTrigger.PROJECT_NAME;

public class ProjectCondition implements AutomationRuleComponent {
    @Override
    public ExecutionResult execute(RuleContext ruleContext, ComponentInputs componentInputs) {
        final String projectName = (String) componentInputs.getInputs().get(PROJECT_NAME);
        if (StringUtils.isBlank(projectName)) {
            ruleContext.getAuditLog().addMessage("com.codebarrel.thirdparty.addon.project.condition.no.project");
            return ExecutionResultBuilder.stopRule();
        }

        final ProjectConditionConfig config = Json.convertObject(ruleContext.getComponentConfigBean().getValue(), ProjectConditionConfig.class);
        if (Pattern.compile(config.getNameRegex()).matcher(projectName).matches()) {
            ruleContext.getAuditLog().addMessage("com.codebarrel.thirdparty.addon.project.condition.match");
            return ExecutionResultBuilder.continueRule();
        } else {
            ruleContext.getAuditLog().addMessage("com.codebarrel.thirdparty.addon.project.condition.no.match");
            return ExecutionResultBuilder.stopRule();
        }
    }
}
