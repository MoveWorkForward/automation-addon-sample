package com.codebarrel.automation.addon.upgrade;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.codebarrel.automation.addon.model.SampleCommentConfig;
import com.codebarrel.automation.addon.util.Json;
import com.codebarrel.automation.api.config.ComponentConfigBean;
import com.codebarrel.automation.api.thirdparty.upgrade.ComponentUpgrade;
import com.codebarrel.automation.api.thirdparty.upgrade.ComponentUpgradeTask;
import com.codebarrel.automation.api.thirdparty.upgrade.LegacyPluggableRuleConfig;

import javax.inject.Named;

/**
 * Please note that ComponentUpgradeTasks *have* to be @ExportAsService annotated, otherwise they wont run!
 */
@Named
@ExportAsService
public class LegacySampleCommentUpgradeTask implements ComponentUpgradeTask {

    @Override
    public String getComponentModuleKey() {
        return "com.codebarrel.automation.addon.automation-legacy-action-sample:sample-comment-issue-action";
    }

    @Override
    public int getSchemaVersion() {
        return 2;
    }

    @Override
    public String getDescription() {
        return "Upgrades the old sample commment legacy action to our newer implementation.";
    }

    @Override
    public ComponentUpgrade doUpgrade(ComponentConfigBean componentConfigBean) {
        // this is when upgrading from the old school legacy components. Their configs are all stored as
        // LegacyPluggableRuleConfig.
        final LegacyPluggableRuleConfig legacyConfig = Json.convertObject(componentConfigBean.getValue(), LegacyPluggableRuleConfig.class);

        final SampleCommentConfig sampleCommentConfig = new SampleCommentConfig(
                legacyConfig.getParameters().get("commentBody").get(0),
                Boolean.parseBoolean(legacyConfig.getParameters().get("sendNotification").get(0)));
        return ComponentUpgrade
                .upgrade(componentConfigBean)
                .setType("com.codebarrel.automation.addon.automation-thirdparty-sample:new-sample-comment-issue-action")
                .setValue(sampleCommentConfig)
                .build();
    }
}
