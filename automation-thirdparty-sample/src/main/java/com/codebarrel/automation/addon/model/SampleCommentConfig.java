package com.codebarrel.automation.addon.model;

public class SampleCommentConfig {
    private String body;
    private boolean sendNotification;

    public SampleCommentConfig() {
    }

    public SampleCommentConfig(String body, boolean sendNotification) {
        this.body = body;
        this.sendNotification = sendNotification;
    }

    public String getBody() {
        return body;
    }

    public boolean isSendNotification() {
        return sendNotification;
    }
}
