package com.codebarrel.automation.addon.model;

import com.codebarrel.automation.api.thirdparty.smartvalues.SmartValueSafe;

public class ProjectIdentity {
    private Long id;
    private String key;

    // Default constructor is required for Jackson!
    public ProjectIdentity() {
    }

    public ProjectIdentity(Long id, String key) {
        this.id = id;
        this.key = key;
    }

    @SmartValueSafe
    public Long getId() {
        return id;
    }

    @SmartValueSafe
    public String getKey() {
        return key;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProjectIdentity{");
        sb.append("id=").append(id);
        sb.append(", key='").append(key).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
