package com.codebarrel.automation.addon.model;

public class ProjectConditionConfig {
    private String nameRegex;

    public ProjectConditionConfig() {
    }

    public ProjectConditionConfig(String nameRegex) {
        this.nameRegex = nameRegex;
    }

    public String getNameRegex() {
        return nameRegex;
    }
}
