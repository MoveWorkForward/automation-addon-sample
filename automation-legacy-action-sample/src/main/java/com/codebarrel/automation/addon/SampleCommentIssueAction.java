package com.codebarrel.automation.addon;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.auditlog.AuditString;
import com.atlassian.plugin.automation.core.auditlog.DefaultAuditString;
import com.atlassian.plugin.automation.core.codebarrel.ComponentConfig;
import com.atlassian.plugin.automation.core.codebarrel.ExecutionContext;
import com.atlassian.plugin.automation.core.codebarrel.RuleConfig;
import com.atlassian.plugin.automation.core.codebarrel.RuleConfigAwareAction;
import com.atlassian.plugin.automation.core.codebarrel.ValidationContext;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;

/**
 * This is an example of an action that implements the new {@link RuleConfigAwareAction} interface that
 * provides access to the rule configuration.  If you don't need this, then you can just implement {@link com.atlassian.plugin.automation.core.Action};
 */
@Scanned
public class SampleCommentIssueAction implements RuleConfigAwareAction<Issue> {
    private static final Logger LOG = Logger.getLogger(SampleCommentIssueAction.class);

    private static final String COMMENT_KEY = "commentBody";
    private static final String SEND_NOTIFICATION = "sendNotification";

    private final UserManager userManager;
    private final IssueService issueService;
    private final CommentService commentService;

    private String comment;
    private boolean sendNotifications = true;
    private final AtomicBoolean executionSuccessful = new AtomicBoolean(true);

    @Inject
    public SampleCommentIssueAction(
            @ComponentImport final CommentService commentService,
            @ComponentImport final UserManager userManager,
            @ComponentImport final IssueService issueService) {
        this.commentService = commentService;
        this.userManager = userManager;
        this.issueService = issueService;
    }

    @Override
    public void init(ActionConfiguration config) {
        comment = singleValue(config, COMMENT_KEY);
        sendNotifications = Boolean.parseBoolean(singleValue(config, SEND_NOTIFICATION));
    }

    @Override
    public void execute(String actor, Iterable<Issue> items, ErrorCollection errorCollection) {
        throw new UnsupportedOperationException("Superseded by execute(ExecutionContext) below");
    }

    @Override
    public ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, String actor) {
        throw new UnsupportedOperationException("Superseded by validateConfiguration(ValidationContext) below");
    }

    @Override
    public String getConfigurationTemplate(ActionConfiguration actionConfiguration, String actor) {
        throw new UnsupportedOperationException("This method should no longer be called in the latest Automation for Jira.");
    }

    @Override
    public String getViewTemplate(ActionConfiguration actionConfiguration, String actor) {
        throw new UnsupportedOperationException("This method should no longer be called in the latest Automation for Jira.");
    }

    @Override
    public void execute(ExecutionContext<Issue> executionContext) {
        LOG.warn("The execution context contains info about the rule: " + executionContext.getRuleConfig());
        // e.g. this is out how you'd get the Scheduled trigger JQL query:
        final Optional<String> jqlOpt = getJql(executionContext.getRuleConfig());
        LOG.warn(String.format("Scheduled trigger Jql: '%s'", jqlOpt.orElse("NONE PROVIDED")));

        final ApplicationUser user = userManager.getUserByName(executionContext.getActor());
        for (Issue issue : executionContext.getItems()) {
            final Issue mutableIssue = issueService.getIssue(user, issue.getId()).getIssue();

            final CommentService.CommentParameters commentParameters = new CommentService.CommentParameters.CommentParametersBuilder()
                    .author(user)
                    .body(comment)
                    .issue(mutableIssue)
                    .build();

            final CommentService.CommentCreateValidationResult validationResult = commentService.validateCommentCreate(user, commentParameters);
            if (validationResult.isValid()) {
                commentService.create(user, validationResult, sendNotifications);
            } else {
                // set this to false to avoid a success message being added. See getAuditLog() below.
                executionSuccessful.set(false);
                executionContext.getErrorCollection().addErrorCollection(toAutomationErrorCollection(validationResult));
            }
        }
    }

    @Override
    public ErrorCollection validateConfiguration(ValidationContext validationContext) {
        LOG.warn("The validation context contains info about the rule: " + validationContext.getRuleConfig());

        // e.g. this is out how you'd get the Scheduled trigger JQL query:
        final Optional<String> jqlOpt = getJql(validationContext.getRuleConfig());
        LOG.warn(String.format("Scheduled trigger Jql: '%s'", jqlOpt.orElse("NONE PROVIDED")));

        final ErrorCollection errorCollection = new ErrorCollection();
        final String commentBody = singleValue(validationContext.getParams(), COMMENT_KEY);
        if (StringUtils.isBlank(commentBody)) {
            errorCollection.addError(COMMENT_KEY, validationContext.getI18n().getText("com.codebarrel.addon.comment.missing"));
        }
        // contrived example.
        if (!StringUtils.contains(commentBody, "dear")) {
            errorCollection.addError(COMMENT_KEY, validationContext.getI18n().getText("com.codebarrel.addon.comment.missing.dear"));
        }
        return errorCollection;
    }

    private Optional<String> getJql(RuleConfig ruleConfig) {
        final ComponentConfig trigger = ruleConfig.getTrigger();
        if (trigger.getComponent().equals("TRIGGER") && trigger.getType().equals("jira.jql.scheduled")) {
            return Optional.ofNullable((String) trigger.getValue().get("jql"));
        }
        return Optional.empty();
    }


    @Override
    @Deprecated
    public AuditString getAuditLog() {
        // this is not ideal, but it's the way the old labs API worked.  Basically we only return an audit log
        // if the execution was successful (this will appear under "Executed successfully:" in the audit log.
        // Errors should be added via the ErrorCollection available in the 'ExecutionContext' in the execute method
        if (executionSuccessful.get()) {
            return new DefaultAuditString(String.format("Added comment '%s'", comment));
        }
        return null;
    }


    private ErrorCollection toAutomationErrorCollection(CommentService.CommentCreateValidationResult validationResult) {
        final ErrorCollection automationErrors = new ErrorCollection();
        automationErrors.addErrorMessages(Sets.newHashSet(validationResult.getErrorCollection().getErrorMessages()));
        validationResult.getErrorCollection().getErrors().forEach(automationErrors::addError);
        return automationErrors;
    }
}
