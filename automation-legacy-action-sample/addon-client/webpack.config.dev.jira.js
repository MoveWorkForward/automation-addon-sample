const path = require('path');

module.exports = {
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
          },
        },
      },
    ],
  },
  entry: {
    samplecomment: './src/sample-comment.js'
  },
  output: {
    path: path.join(__dirname, '../src/main/resources/client'),
    filename: '[name].pack.js',
  },
  externals: {
    /* These are included on the page already by Automation for Jira. */
    react: 'React',
    'react-dom': 'ReactDOM',
    'styled-components': 'window.CodeBarrel.StyledComponents',
  },
  devtool: 'cheap-source-map',
  mode: 'development',
};