import SampleCommentConfigForm from './sample-comment-config';

const SampleCommentComponent = {
  /** This has to match the server side 'automation-action' complete module key: <pluginkey>:<automation-action-modulekey> */
  type: 'com.codebarrel.automation.addon.automation-legacy-action-sample:sample-comment-issue-action',

  /** Currently only 'ACTION' is supported */
  componentType: 'ACTION',

  /** this renders the name in the header of the config form and when selecting a new action */
  name: () => AJS.I18n.getText('com.codebarrel.addon.comment.name'),

  /**
   * Use SVG icons only please.  Transparent background 36x36 pixels with white line color only.  Your icon should *not* look out of place
   * or stand out in any special way compare to other icons used in Automation for Jira!
   */
  icon: () => '../com.codebarrel.automation.addon.automation-legacy-action-sample:sample-comment-action-resources/images/sample-icons/comment.svg',

  /** Components only support values of the form { parameters: { yourKey: [yourValue(s)] } }. */
  initialValue: () => ({ parameters: { commentBody: [''], sendNotification: ['true'] } }),

  getRenderer: () => ({
    /** this renders the header in the sidebar.  Then: xxxx */
    summaryLabel: () => AJS.I18n.getText('com.codebarrel.addon.comment.summary'),
    /** This renders the contents of the panel in the sidebar. Should show short summary of configured options */
    renderSummary: (config) => {
      let value = '';
      if (config.value && config.value.parameters.commentBody[0]) {
        value = config.value.parameters.commentBody[0];
      }
      return <div title={value}>{value}</div>;
    },
    /** This renders the detailed config form on the right hand side in the rule builder */
    renderDetailed: (component) => <SampleCommentConfigForm component={component} />,
    /** this renders the description when selecting a new action. Should be short description. */
    renderCard: () => <span>{AJS.I18n.getText('com.codebarrel.addon.comment.card')}</span>,
  }),

  /**
   * Us this to carry out client side validation. You must also validate on the server, but this
   * is useful to give the user immediate feedback when they click 'Save' client-side before publishing
   */
  validate: (rule, config) => {
    const commentBody = config.value.parameters.commentBody[0];
    //this is a contrived example!
    if (commentBody.indexOf('Hello') === -1) {
      return { commentBody: AJS.I18n.getText('com.codebarrel.addon.comment.client.error') }
    }
    return { commentBody: null };
  },

  /**
   * Set this to false, if this component doesn't require a config UI.  'renderDetailed' will not need to be implemented if this is false.
   */
  isEditable: true,

  /**
   * Valid categories are: 'ISSUE_ACTIONS','SERVICE_DESK_ACTION','ISSUE_TRIGGERS','NOTIFICATIONS','SCHEDULED','INTEGRATIONS'
   * This is used for the search dropdown in the select action ui.
   */
  categories: [],

  /**
   * Add additional labels to aid searching.  These wont appear in the UI, but will match text search when selecting an action.
   * Only use labels that aren't included already in your name or card.
   */
  labels: [],
};

window.CodeBarrel.Automation.registerComponent(SampleCommentComponent);
