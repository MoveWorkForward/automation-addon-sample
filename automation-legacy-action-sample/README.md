# Automation for Jira (LEGACY) extension developer guide

## This guide contains an example implementation of the old legacy APIs provided by the outdated Atlassian Labs plugin. Newer apps should no longer use this, but instead follow the `automation-thirdparty-sample` guide.

## tl;dr

* Run `atlas-debug`
* Access [http://localhost:2990/jira](http://localhost:2990/jira) and follow the instructions in the banner!
* Make a code change, then run `atlas-package` in another terminal to re-deploy. (this will trigger a reload via quickreload)

*Note:* For the last step to work correctly for front-end code changes you may need to disable browser caches, 
or hit the 'b' keyboard shortcut in Jira to turn off web-resource batching and caching.

## For the more patient

This guide is meant for third party add-on developers that want to extend 
[Automation for Jira](https://marketplace.atlassian.com/plugins/com.codebarrel.addons.automation/) 
with custom actions. 
 
Extensions will be possible for both Automation Lite for Jira Server as well as Automation for Jira for Server.
Cloud extensions will be added at some stage in future, but they will be based on a different technology stack to p2.

Please note that in Automation Lite for Jira server, only extensions whitelisted by CodeBarrel will be configurable by users.
We are no longer adding new apps to this whitelist and only continuing to support existing extensions that used to exist
in the old Jira Automation labs plugin. We provide Automation Lite for Jira Server free of charge, yet bear the costs to 
support it for a large customer base and with more extensions comes a higher support cost.
  
We also highly encourage vendors out there to not build third party extensions that duplicate our paid functionality or 
are on our road map.  If in doubt, please talk to us first!

In the paid version of Automation for Jira server any third party extension is allowed.

## Prerequisites
There's a number of tools you'll need to develop an extension:

* Yarn: https://yarnpkg.com/en/
* Atlassian plugin SDK: https://developer.atlassian.com/docs/getting-started

Some working knowledge of Webpack, React, ES6 and npm/yarn package managers would also be useful.

## What's in this add-on?
This add-on demonstrates an example extension that adds a comment to an issue, with some contrived validation.

Specifically this demonstrates:

* How to write the server-side component implementing the [Action API](https://bitbucket.org/codebarrel/automation-public/src/e1323208b7aeee7c3912b56d439c26d25debf6b5/automation-api/src/main/java/com/atlassian/plugin/automation/core/codebarrel/RuleConfigAwareAction.java?at=master&fileviewer=file-view-default)
* How to write the client-side React components implementing the new UI for Automation for Jira (including i18n)

The server-side Action SPI can be added with this dependency in your `pom.xml`:

```
<dependency>
    <groupId>com.atlassian.plugin.automation</groupId>
    <artifactId>automation-api</artifactId>
    <version>2.2.0</version>
    <scope>provided</scope>
</dependency>
```

You will also have to add our public maven repository to your `pom.xml` to get this JAR:
```
<repositories>
    <repository>
        <id>maven-public.codebarrel.io</id>
        <url>https://maven-public.codebarrel.io/public</url>
    </repository>
</repositories>
```
(if this doesn't work, you can download the jar directly via https://s3.amazonaws.com/maven-public.codebarrel.io/public/com/atlassian/plugin/automation/automation-api/2.2.0/automation-api-2.2.0.jar )


The source for this public API is available here: https://bitbucket.org/codebarrel/automation-public

The key parts are registering the server-side Action implementation in the `atlassian-plugin.xml` using a 
`<automation-action/>` module descriptor. Next the client-side UI needs to implement the rule component interface 
documented in detail in the provided `sample-comment.js`.  One important bit here is that the `type` attribute
of this component has to match the complete module key of the `<automation-action/>` for the server component.

It's how we link the client-side UI to the server side component.

For more details on how to implement the client-side, please view the `addon-client` specific [README](https://bitbucket.org/codebarrel/automation-addon-sample/src/HEAD/addon-client/README.md).

## Development cycle
Development should be pretty straightforward. Initially to get things running:

* Start your add-on with `atlas-debug`
* That's it. Your add-on should now be up and running and you should see it in the Automation UI.
* Run `atlas-package` to trigger a re-install of your add-on.

Continuous development:

* Make a code change
* In your add-on directory run `atlas-package`.  Quickreload should then automatically reload your add-on
* Refresh in the browser.

*Note* You may want to switch the `exec-yarn-build` goal in the `pom.xml` file to use `dev-jira` webpack build to avoid 
js minification for easier debugging in the browser. For more complex UIs you may want to run with webpack dev server and hot 
  reloading but that's left as an exercise for the reader ;).

## Questions?

Contact us at [https://codebarrel.io/support](https://codebarrel.io/support)!